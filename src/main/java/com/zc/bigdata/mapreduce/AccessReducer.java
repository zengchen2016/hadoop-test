package com.zc.bigdata.mapreduce;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class AccessReducer extends Reducer<Text, Access, Text, Access> {

    @Override
    protected void reduce(Text key, Iterable<Access> values, Context context) throws IOException, InterruptedException {
        long up = 0, down = 0;
        for (Access value : values) {
            up += value.getUp();
            down += value.getDown();
        }
        context.write(key, new Access(key.toString(), up, down));
    }
}
